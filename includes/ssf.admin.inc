<?php

/**
 * @file
 * Simple System Settings configurable form and export/import functionality.
 */

/**
 * System Settings configurable form.
 */
function ssf_form($form, &$form_state) {
  $stored_data = db_select('ssf_variables', 'sv')
    ->fields('sv', array('ssf_key', 'ssf_value'))
    ->execute()->fetchAll(PDO :: FETCH_ASSOC);

  $form['#tree'] = TRUE;
  $form['variable_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('SSF Variables'),
    '#prefix' => '<div id="textbox-wrapper">',
    '#suffix' => '</div>',
  );
  if (isset($stored_data) && !empty($stored_data)) {
    $count_num = count($stored_data) + 1;
  }
  else {
    $count_num = 1;
  }
  $form_state['num_text'] = empty($form_state['num_text']) ? $count_num : $form_state['num_text'];
  for ($i = 0; $i < $form_state['num_text']; $i++) {
    $form['variable_data'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t("SSF Variable - @data", array('@data' => ($i + 1))),
      '#prefix' => '<div id="variable-wrapper">',
      '#suffix' => '</div>',
    );

    $key_description = empty($stored_data[$i]) ? '' : t("variable_get('@data');", array('@data' => $stored_data[$i]['ssf_key']));
    $form['variable_data'][$i]['variable_key'] = array(
      '#type' => 'textfield',
      '#title' => t('key'),
      '#disabled' => empty($stored_data[$i]) ? FALSE : TRUE,
      '#description' => check_plain($key_description),
      '#size' => 30,
      '#required' => TRUE,
      '#default_value' => empty($stored_data[$i]) ? '' : substr($stored_data[$i]['ssf_key'], 4),
    );
    $form['variable_data'][$i]['variable_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Value'),
      '#size' => 80,
      '#default_value' => empty($stored_data[$i]) ? '' : $stored_data[$i]['ssf_value'],
    );

    if (!empty($stored_data[$i])) {
      $form['variable_data'][$i]['variable_delete'] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete'),
        '#prefix' => '<div class="wrap-right">',
        '#suffix' => '</div>',
      );
    }
  }
  $form['variable_data']['add_textbox'] = array(
    '#type' => 'submit',
    '#value' => t('Add one more'),
    '#submit' => array('ssf_add_one_more'),
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'callback' => 'ssf_add_one_more_callback',
      'wrapper' => 'textbox-wrapper',
    ),
  );

  if ($form_state['num_text'] > ($count_num - 1) && $form_state['num_text'] > 1) {
    $form['variable_data']['delete_textbox'] = array(
      '#type' => 'submit',
      '#value' => t('Delete last one'),
      '#submit' => array('ssf_delete_last'),
      '#limit_validation_errors' => array(),
      '#ajax' => array(
        'callback' => 'ssf_delete_last_callback',
        'wrapper' => 'textbox-wrapper',
      ),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Data',
    '#weight' => 1000,
  );
  if ($form_state['num_text'] > 1) {
    $form['delete_selected'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Selected'),
      '#submit' => array('ssf_delete_selected'),
      '#limit_validation_errors' => array(),
      '#ajax' => array(
        'callback' => 'ssf_delete_selected_callback',
        'wrapper' => 'textbox-wrapper',
      ),
      '#prefix' => '<div class="wrap-right">',
      '#suffix' => '</div>',
    );
  }
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'ssf') . '/css/ssf_form.css' => array(
      'group' => CSS_DEFAULT,
      'every_page' => FALSE,
    ),
  );
  return $form;
}

/**
 * Logic for add more element.
 */
function ssf_add_one_more($form, &$form_state) {
  $form_state['num_text']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Add more callback function.
 */
function ssf_add_one_more_callback($form, &$form_state) {
  return $form['variable_data'];
}

/**
 * Logic for delete last element.
 */
function ssf_delete_last($form, &$form_state) {
  $form_state['num_text']--;
  $form_state['rebuild'] = TRUE;
}

/**
 * Delete last element callback function.
 */
function ssf_delete_last_callback($form, $form_state) {
  return $form['variable_data'];
}

/**
 * Function ssf_delete_selected() which rebuilds form_state.
 */
function ssf_delete_selected($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Delete selected element callback function.
 */
function ssf_delete_selected_callback($form, $form_state) {
  $variable_data = $form_state['values']['variable_data'];
  unset($variable_data['add_textbox'], $variable_data['delete_textbox']);
  foreach ($variable_data as $value) {
    if ($value['variable_delete'] == 1) {
      $variable_key = 'ssf_' . $value['variable_key'];
      db_delete('ssf_variables')
        ->condition('ssf_key', $variable_key)
        ->execute();
      variable_del($variable_key);
    }
  }
  drupal_set_message(t('Seleceted SSF variable data has been deleted successfully.'));
  $path = "admin/config/ssf-config";
  ctools_include('ajax');
  ctools_add_js('ajax-responder');
  $commands[] = ctools_ajax_command_redirect($path);
  print ajax_render($commands);
  exit;
}

/**
 * Submit function for ssf_form().
 */
function ssf_form_submit($form, &$form_state) {
  $variable_data = $form_state['values']['variable_data'];
  unset($variable_data['add_textbox'], $variable_data['delete_textbox']);
  foreach ($variable_data as $value) {
    $variable_key = 'ssf_' . $value['variable_key'];
    $variable_key = strtolower(str_replace(' ', '_', $variable_key));
    $variable_value = $value['variable_value'];
    db_merge('ssf_variables')
      ->key(array('ssf_key' => $variable_key))
      ->fields(array(
        'ssf_value' => $variable_value,
      ))
      ->execute();
    variable_set($variable_key, $variable_value);
  }
  drupal_set_message(t('SSF variable data has been saved successfully.'));
}

/**
 * Adds logic to validate the ssf_form to check the validity of unique keys.
 */
function ssf_form_validate($form, &$form_state) {
  $variable_data = $form_state['values']['variable_data'];
  unset($variable_data['add_textbox'], $variable_data['delete_textbox']);
  $variable_keys = array();
  foreach ($variable_data as $key => $value) {
    $variable_keys[] = $value['variable_key'];
    // Check for special characters.
    if (!preg_match("/^[a-z,A-Z,0-9,_,  ]+$/i", $value['variable_key'])) {
      form_set_error('variable_data][' . $key . '][variable_key', t('Key field cannot contain special characters (except underscores).'));
    }
  }
  // Unique key validation.
  $unique = array_unique($variable_keys);
  $duplicates = array_diff_assoc($variable_keys, $unique);
  $duplicate_keys = array_keys(array_intersect($variable_keys, $duplicates));
  if (!empty($duplicate_keys)) {
    form_set_error('', t("Variable key's must be unique..."));
    foreach ($duplicate_keys as $value) {
      form_set_error('variable_data][' . $value . '][variable_key', t("for SSF VARIABLE - @data", array('@data' => ($value + 1))));
    }
  }
}

/**
 * SSF data export form.
 */
function ssf_export_data($form, $form_state) {
  $stored_data = db_select('ssf_variables', 'sv')
    ->fields('sv', array('ssf_key', 'ssf_value'))
    ->execute()->fetchAll(PDO :: FETCH_ASSOC);
  $export_data = json_encode($stored_data);
  $form['variable_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('SSF Variables'),
    '#prefix' => '<div id="textbox-wrapper">',
    '#suffix' => '</div>',
  );
  if (!empty($stored_data)) {
    $form['variable_data']['content'] = array(
      '#type' => 'textarea',
      '#title' => t('SSF Variables Data Export'),
      '#description' => t("Copy the above generated data in the SSF configuration importer."),
      '#prefix' => '<div id="variable-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $export_data,
    );
  }
  else {
    $message = '<p> Looks like their is nothing to export, '
        . 'add some variables in SSF configuration to export them.</p>';
    $form['variable_data']['message'] = array('#markup' => $message);
  }
  return $form;
}

/**
 * SSF data import form.
 */
function ssf_import_data($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form['variable_data'] = array(
      '#type' => 'fieldset',
      '#title' => t('SSF Variables'),
      '#prefix' => '<div id="textbox-wrapper"> ',
      '#suffix' => '</div>',
    );
    $form['variable_data']['content'] = array(
      '#type' => 'textarea',
      '#title' => t('SSF Variables Data Import'),
      '#prefix' => '<div id="variable-wrapper">',
      '#suffix' => '</div>',
      '#required' => TRUE,
    );
    $message = '<h4>Note:</h4>
    <p> This import will delete all the existing SSF variable data (if present).</p>';
    $form['variable_data']['message'] = array('#markup' => $message);
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Import Data',
      '#weight' => 50,
    );
    return $form;
  }
  else {
    $message = '<h4>Import Confirmation !!</h4>
    <p> This import will delete all the existing SSF variable data (if any).</p>';
    $message .= '<p> Are you sure you want to continue?</p>';
    $form['message'] = array('#markup' => $message);
    return confirm_form(
        $form, t('Are you sure you want to import this data?'), 'admin/config/ssf-config/import', t('This action cannot be undone.'), t('Confirm'), t('Cancel')
    );
  }
  return $form;
}

/**
 * Logic to validate the ssf importer to check the validity of supplied data.
 */
function ssf_import_data_validate($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $json_data = json_decode($form_state['values']['content']);
    if (is_null($json_data) || (empty((array) $json_data)) || empty($json_data)) {
      form_set_error('content', t('Invalid Data, Provide valid data generated by SSF configuration export.'));
    }
    foreach ($json_data as $value) {
      $variable_key = $value->ssf_key;
      if (!(isset($variable_key) && (strlen(trim($variable_key)) > 0))) {
        form_set_error('content', t("Invalid Data : Variable key can't be empty"));
      }
      $required_structure = array('ssf_key', 'ssf_value');
      $json_keys = array_keys((array) $value);
      if ($required_structure != $json_keys) {
        form_set_error('content', t('Invalid Data : Unexpected Data Format, Provide valid data generated by SSF configuration export.'));
        break;
      }
    }
  }
}

/**
 * Submit function for ssf_import_data().
 */
function ssf_import_data_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['values'] = $form_state['values'];
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    // Delete the previously saved ssf variables.
    $stored_data = db_select('ssf_variables', 'sv')
      ->fields('sv', array('ssf_key', 'ssf_value'))
      ->execute()->fetchAll(PDO :: FETCH_ASSOC);

    foreach ($stored_data as $value) {
      variable_del($value['ssf_key']);
    }
    db_truncate('ssf_variables')
      ->execute();
    $json_data = json_decode($form_state['storage']['values']['content']);
    foreach ($json_data as $value) {
      $variable_key = $value->ssf_key;
      $variable_value = $value->ssf_value;
      db_insert('ssf_variables')
        ->fields(array(
          'ssf_key' => $variable_key,
          'ssf_value' => $variable_value,
        ))
        ->execute();
      variable_set($variable_key, $variable_value);
    }
    drupal_set_message(t('SSF variable data has been imported successfully.'));
    $form_state['redirect'] = 'admin/config/ssf-config';
  }
}
