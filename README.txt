
SSF (Simple settings form)
==========================
The module contains SSF (Simple settings form) implementation, which provides 
configurable system settings form.   


Main features:
- Configurable system settings form
 - Export Configurable settings
 - Import Configurable settings

Required Modules
================
- ctools

Installation
============
* Go to the Modules page (/admin/modules) and enable it.

Configuration:
==============
Go to "SSF configuration" page (admin/config/ssf-config).
->Add Variable key and Value.
->Access them using variable_get('ssf_{key})');

Import/Export Configuration:
===========================
Go to "Export SSF configuration" page (admin/config/ssf-config/export).
->Copy the generated data in "Import SSF configuration" page
 i.e (admin/config/ssf-config/import).
